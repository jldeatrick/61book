\documentclass[11pt]{exam}

\usepackage[utf8]{inputenc}

\usepackage{titling}

\usepackage{amsmath}

\usepackage{graphicx}

\graphicspath{ {../../images/} }

\usepackage{hyperref} 

\hypersetup{ colorlinks = true, urlcolor=blue }

\setlength{\droptitle}{-7.25em}

\newcommand{\newpar}{\medskip\noindent}


\author{CS 61BL Summer 2019\qquad \includegraphics [scale=0.1]{blear.png}}
\date{\vspace{-10ex}}
\title{From Dijkstra's to A* and Consistent Heuristics}

\begin{document}
\maketitle
\noindent\rule{17cm}{0.4pt}

\section*{Dijkstra's and A*}
We have seen in class two very similar algorithms for computing shortest paths: \textit{A*} and \textit{Dijkstra's}. Recall that running Dijkstra's from a source node $s$ actually computes the shortest path from $s$ to any other node connected to $s$. However, if we simply want the shortest path from Berkeley to Pacifica, we needn't calculate the shortest path to Tel Aviv as well. In many cases we only care about the shortest path from $s$ to $t$, some specific target node. 

\medskip
In these cases, we can modify Dijkstra's to immediately terminate once we dequeue $t$, and just return the path from $s$ to $t$. This algorithm is known as \textit{Uniform Cost Search}, and has wide-reaching applications in Artificial Intelligence. This is a good step, but only alters the algorithm's runtime from $\Theta(ElogE)$ to $\mathcal{O}(ElogE)$, as in the worst case $t$ will be the last node dequeued.

\medskip
We can further optimize this algorithm by prioritizing nodes closer to $t$, introducing a \href{https://en.wikipedia.org/wiki/Heuristic}{heuristic function} $h$ to determine a node's priority. $h$ takes a node $x$ as input and returns an estimate of the path cost from $x$ to $t$. The priority of $x$ then becomes the cost of going from $s$ to $x$ plus $h(x)$. More rigorously, $\texttt{priority}(x) := \texttt{pathCost}(s,x) + h(x)$. This algorithm will still calculate the shortest path from $s$ to $t$, but visits far fewer nodes on average, yielding impressive runtime improvement. This second optimization gives us A*.

\section*{Pseudocode}
Let us observe at a finer granularity, the key differences between Dijkstra's and A*. In both algorithms we begin by enqueueing $s$, our start node. In Dijkstra's, we will enqueue $s$ with a priority of $0$, but in A* we will enqueue it with priority $h(s)$. We then repeat these steps: 

\begin{enumerate} 
    \item Pop (and visit) the minimum-priority node $x$ from the $\texttt{PriorityQueue}$, whose priority was $p$ 
    \item Terminate if: 
    \begin{itemize}
        \item {\bf (Dijkstra's)} the $\texttt{PriorityQueue}$ is empty 
        \item {\bf (A*)} $x$ is our goal node $t$
    \end{itemize} 
    \item For each neighbor $y$ of $x$, If $y$ is not visited, queue $y$ with priority $q$, where: 
        \begin{itemize} 
            \item {\bf (Dijkstra's)} $q = \texttt{pathCost}(s,x) + \texttt{edgeCost}(x,y) = p + \texttt{edgeCost}(x,y)$
            \item {\bf (A*)} $q = \texttt{pathCost}(s,x) + \texttt{edgeCost}(x,y) + h(y) = p - h(x) + \texttt{edgeCost}(x,y) + h(y)$ 
        \end{itemize}
\end{enumerate}
   
\section*{Tree Search and Graph Search} 

\newpar
Note the above implementation of Dijkstra's and A* are instances of $\textit{Graph Search}$, where nodes are explored at most once. There is also A* $\textit{Tree Search}$, allowing such re-exploration. For A* to return the optimal path from $s$ to $t$, we have the following restrictions on $h$: 

\begin{enumerate} 
    \item For Tree Search, $h$ must be $\textit{admissible}$, meaning:
    
    For every node $x:\;h(x) \leq \texttt{pathCost}(x, t)$
    
    \item for Graph Search, $h$ must be $\textit{consistent}$, meaning:
    
    For every pair of adjacent nodes $x,y:\;\texttt{edgeCost}(x,y) \geq h(x) - h(y)$
    
\end{enumerate}

\section*{More on Consistency}
Since A* as described above is an optimization of Dijkstra's, it is restricted in similar ways. Recall that Dijkstra's may fail to return the optimal path from $s$ to every other node when the graph has negative edge weights. This occurs because the optimality of Dijkstra's depends on path costs increasing as you add edges. Check \href{https://stackoverflow.com/questions/13159337/why-doesnt-dijkstras-algorithm-work-for-negative-weight-edges#13159425}{this link} out if you want to see further discussion of this.

\medskip
In short, Dijkstra's may not explore a path containing an edge with incredibly large negative weight because it is hidden behind a large positive one. Similarly, in A*, if $h$ is inconsistent, we may find $t$ through a less-optimal path, because our estimates were inaccurate and obstructive. The graph below has an admissible but inconsistent $h$, as $\texttt{edgeCost}(B,A) = 1 < 4 = h(B) - h(A)$

 \begin{center}\includegraphics [scale=0.4]{path.jpg}\end{center}
 
 A* Graph Search starting at $S$ would return the path $S-A-G$ as $h$ grossly underestimates $\texttt{pathCost}(A,G)$, meaning A* pops $A$ before $B$. This means we are unable to revisit $A$ through $B$ (as $A$ has already been visited) and find the more optimal path $S-B-A-G$. Notice that Tree Search would find the correct path since $h$ is admissible and $A$ could be revisited from $B$. If this remains unclear, I suggest simply running A* on the graph as-is, and then rerunning it after changing $h(A)$ to $4$. This makes $h$ becomes consistent and A* Graph Search finds the optimal path.
 
 \medskip
 One may find it useful to think of Dijkstra's as a special case of A*. Given some graph $G$, one could simply alter each edge weight, changing $\texttt{edgeCost}(x,y)$ to $\texttt{edgeCost}(x,y) - h(x) + h(y)$. Running Dijkstra's on this new graph is the same as running A* on the original graph. It may be helpful to do this for the graph above and verify this claim for yourself. Given this fact, in this instance of Dijkstra's we have that for an optimal solution, our edge costs must be non-negative. In particular, we would have $\texttt{edgeCost}(x,y) - h(x) + h(y) \geq 0$, meaning $h(x) - h(y) \leq cost(x, y)$. This is exactly the definition of a consistent heuristic, meaning the requirement for non-negative edge weights in Dijkstra's and a consistent heuristic in A* are one-in-the-same.

\end{document}