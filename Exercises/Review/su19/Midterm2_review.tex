\documentclass[11pt]{exam}

\usepackage[utf8]{inputenc}
\usepackage{forest}

\newif\ifsolutions
% Toggle to include/exclude solutions
% \solutionstrue      

\usepackage{titling}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}
\usepackage{tikz}
\usepackage{float}
\usepackage{hyperref} 
\usepackage{multicol}
\hypersetup{ colorlinks = true, urlcolor=blue }

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstloadlanguages{Java}
\lstset{language=Java,
        frame=single,
        basicstyle=\small\ttfamily,
        identifierstyle=,
        showstringspaces=false,
        tabsize=4,
        numbers=left,
        numberstyle=\tiny,
        breaklines=true,
}

\graphicspath{ {../../../images/} }

\setlength{\droptitle}{-8.25em}
\setlength{\tabcolsep}{5mm} % separator between columns
\def\arraystretch{1.25} % vertical stretch factor


\usetikzlibrary{circuits.logic.US,circuits.logic.IEC,fit,trees,arrows,calc,shapes.multipart,chains,arrows,trees}
\newcommand\addvmargin[1]{
  \node[fit=(current bounding box),inner ysep=#1,inner xsep=0]{};
}

\tikzset{
  treenode/.style = {align=center, inner sep=0pt, text centered,
    font=\sffamily},
  max/.style = {treenode, regular polygon, regular polygon sides=3, black, font=\sffamily\bfseries, draw=black,
    fill=white, text width=1.6em},
  min/.style = {treenode, regular polygon, regular polygon sides=3, shape border rotate=180, black, font=\sffamily\bfseries, draw=black,
    fill=white, text width=1.6em},
  val/.style = {treenode, regular polygon, regular polygon sides=4, black, font=\sffamily\bfseries, draw=black,
    fill=white, text width=1.8em},
  exp/.style = {treenode, regular polygon, regular polygon sides=5, shape border rotate=180, black, font=\sffamily\bfseries, draw=black,
    fill=white, text width=2em}
}


\author{CS 61BL Summer 2019\qquad \includegraphics [scale=0.1]{blear.png}}
\date{\vspace{-10ex}}
\title{Midterm 2 Review}

\begin{document}
\maketitle
\noindent\rule{17cm}{0.4pt}\\
\section{Introduction}
We've covered quite a lot of material in this course; too much, in fact, to fit on a single worksheet. The focus of this worksheet will be post-Midterm 1 material, but will undoubtedly involve other material as well. Here's a recap of the bigger ideas we've looked at since our last midterm:
\begin{multicols}{2}
\begin{enumerate}
\item Trees \& Tree Traversal
\item Binary Search Trees
\item Comparables \& Comparators
\item B-Trees \& LL{\color{red} R}Bs
\item Game Trees
\item Exceptions \& Iterators
\item Hashing  \& HashMaps
\item Heaps and Priority Queues
\end{enumerate}
\end{multicols}
\noindent
This worksheet will attempt to cover the more challenging of these topics, but is not exhaustive. It is your responsibility to reinforce your understanding of the topics not covered here. You can do it!\\\\
\centerline{\bf \begin{Large}***Work through the problems below in a group of 3-5***\end{Large}}\\
\section{Traversal Town}
Recall the following implementation for an in-order traversal for Binary Trees:

\begin{lstlisting}
public void inOrder(Node n) {
	if (n == null) {return;}
	inOrder(n.left);
	action(n);
	inOrder(n.right);
}
\end{lstlisting}

\noindent
where $\texttt{action}$ is some method acting on n (i.e. something like $\texttt{System.out.println(n.item)}$). Let's make our action a full pre-order traversal of the Tree rooted at n. In other words, say we replace line 4 above with $\texttt{preOrder(n)}$, defined as follows:

\begin{lstlisting}
public void preOrder(Node n) {
	if (n == null) {return;}
	System.out.println(n.item);
	preOrder(n.left);
	preOrder(n.right);
}
\end{lstlisting}

\newpage
\noindent
List the output of running $\texttt{inOrder}$ on the root of each of the following trees, after replacing line 4.

\begin{table}[h]
\begin{tabular}{|l|l|}
\hline
 \begin{tikzpicture}
\node[circle,draw](z){$1$}
  child{node[circle,draw]{2} 
  			child[missing] 
  			child{node[circle,draw]{5} 
  						child[missing] child[missing]}}
  child{
    node[circle,draw]{3} child[missing] child{node[circle,draw] {4}} };
\addvmargin{1mm}
\end{tikzpicture} & output: \hspace{4in} \\ \hline
 \begin{tikzpicture}
\node[circle,draw](z){$1$}
  child{node[circle,draw]{2} 
  			child[missing]
  			child[missing]}
  child{
    node[circle,draw]{3} child{node[circle,draw]{5} 
  						child[missing] child[missing]}
  						child{node[circle,draw] {4}} };
\addvmargin{1mm}
\end{tikzpicture} & output: \hspace{4in} \\ \hline
 \begin{tikzpicture}
\node[circle,draw](z){$1$}
  child{node[circle,draw]{2} 
  			child{node[circle,draw]{5} 
  						child[missing] 
  						child{node[circle,draw]{4} 
  						        child[missing] 
  						        child[missing]}} 
  			child{node[circle,draw]{6} 
  						child[missing] child[missing]}}
  child{
    node[circle,draw]{3} child[missing] child[missing] };
\addvmargin{1mm}
\end{tikzpicture} & output: \hspace{4in} \\ \hline
\end{tabular}
\end{table}

\newpage

\section{Kabbalah}
Observe the following generic BST class with a yet-to-be-implemented method, $\texttt{balance}$. The method will take a possibly unbalanced Binary Search Tree and balance it.
\begin{lstlisting}
public class BST<T extends Comparable<T>> {

    private TreeNode root;

    private class TreeNode {
        private T item;
        private TreeNode left, right;
        private TreeNode(T item) {
            this.item = item;
        }
    }
	
	public void balance() {...}
}
\end{lstlisting}

\noindent
We will implement the above method step-by-step, starting by first implementing the following method, $\texttt{listElements}$, which fills an input list with the elements of the BST in ascending order. 

\ifsolutions{ \color{red}
\begin{lstlisting}
	public List<T> listElements() {
        ArrayList<T> list = new ArrayList<>();
        listElements(list, root);
        return list;
    }

    private void listElements(List<T> list, TreeNode node) {
        if (node == null) { return; }
        listElements(list, node.left);
        list.add(node.item);
        listElements(list, node.right);
    }
\end{lstlisting}
}
\else{
\begin{lstlisting}
	public List<T> listElements() {
        List<T> list = ___________________________;
        listElements(__________, __________);
        return __________;
    }

    private void listElements(List<T> list, TreeNode node) {
        if (_______________) { return; }
        ______________________________;
        ______________________________;
        ______________________________;
    }
\end{lstlisting}
}\fi
\noindent
We will now use the above method to fully implement the balance method below, which will convert the current BST into a balanced BST with the same elements. The number of nodes in the tree is not necessarily a perfect power of two, and thus the resulting tree will not be "perfectly" balanced.

\ifsolutions{ \color{red}
\begin{lstlisting}
	public void balance() {
        List<T> list = listElements();
        root = balance(list, 0, list.size());
    }

    private TreeNode balance(List<T> list, int low, int high) {
        if (high <= low) { return null; }
        int mid = low + ((high - low)/2);
        TreeNode midNode = new TreeNode(list.get(mid));
        midNode.left = balance(list, low, mid);
        midNode.right = balance(list, mid + 1, high);
        return midNode;
    }
\end{lstlisting}
}
\else{
\begin{lstlisting}
	public void balance() {
        List<T> list = listElements();
        ______________________________;
    }

    private TreeNode balance(List<T> list, int low, int high) {
        if (_______________) { return null; }
        _____________________________________________;
        ____________________________________________________________;
        _____________________________________________;
        ____________________________________________________________;
        return _______________;
    }
\end{lstlisting}
}\fi

\noindent
Like good computer scientists, we will now analyze the runtime of the methods we just wrote. For each of the questions below, give a bound on the runtime of the method in $\Theta(\cdot)$ notation and provide justification. Perhaps unsurprisingly, your choice of list implementation on line 2 of $\texttt{listElements}$  may affect the runtime of $\texttt{listElements}$ and $\texttt{balance}$, so both are included in the questions below.

\medskip\noindent

\begin{enumerate}
	\item $\texttt{listElements}$ (using an ArrayList):
		\begin{enumerate}
			\item \medskip
					Runtime: $\Theta(\rule{30pt}{.3pt})$ 

			\item \medskip
					Justification:
					$\vspace{40pt}$
		\end{enumerate}
	\item $\texttt{listElements}$ (using a LinkedList):
		\begin{enumerate}
			\item \medskip
					Runtime: $\Theta(\rule{30pt}{.3pt})$ 

			\item \medskip
					Justification:
					$\vspace{40pt}$
		\end{enumerate}
	\item $\texttt{balance}$ (using an ArrayList): 
		\begin{enumerate}
			\item \medskip
					Runtime: $\Theta(\rule{30pt}{.3pt})$ 

			\item \medskip
					Justification:
					$\vspace{40pt}$
		\end{enumerate}
	\item $\texttt{balance}$ (using a LinkedList): 
		\begin{enumerate}
			\item \medskip
					Runtime: $\Theta(\rule{30pt}{.3pt})$ 

			\item \medskip
					Justification:
					$\vspace{40pt}$
		\end{enumerate}
\end{enumerate}
\newpage

\section{Up Up Down Down Left Right Left Right B A Start Trees}
\input{gameTree.tex}
\newpage
\section{The Leaning Tower of Treeza (LLRBs, BTrees)}
List a possible sequence of insertions that leads to the following 2-3 Tree. 

\bigskip
\noindent
There may be multiple correct answers.

\bigskip

\begin{center}
\begin{tikzpicture}
\tikzstyle{bplus}=[rectangle split, rectangle split horizontal,rectangle split ignore empty parts,draw]
\tikzstyle{every node}=[bplus]
\tikzstyle{level 1}=[sibling distance=60mm]
\tikzstyle{level 2}=[sibling distance=15mm]
\node {15} [->]
  child {node {3 \nodepart{two} 7}
    child {node {1 \nodepart{two} 2}}
    child {node {4 \nodepart{two} 6}}
    child {node {8 \nodepart{two} 9}}    
  } 
  child {node {21 \nodepart{two} 27 }
    child {node {17 \nodepart{two} 20}}
    child {node {22 \nodepart{two} 25}}
    child {node {28 \nodepart{two} 30}}   
  }
;\end{tikzpicture}
\end{center}

\vspace{120pt}
\noindent
\textbf{Sequence of Insertions:}

\vspace{50pt}
\hline

\bigskip
\noindent
Now convert the above 2-3 Tree to an LLRB. Label each red node with an asterisk.

\newpage
\section{Oh, okay Mrs. Fritz I'm boutta Heap out}
Observe the following MinHeap class inspired from our work in lab 14:
\bigskip
\begin{lstlisting}
public class MinHeap<E extends Comparable<E>> {

    private E[] contents;
    private int size;

    public MinHeap() {
        contents = (E[]) new Comparable[8]; /* arbitrary initial size */
    }

    private void bubbleUp(int index) {...}
    private void bubbleDown(int index) {...}

    public void insert(E element) {...}
    public E removeMin() {...}
    public E findMin() {...}
    public int size() {return size;}
}
\end{lstlisting}

\bigskip
\noindent
Assume all the above methods work correctly, and that resizing is done appropriately. As well, the structure of $\texttt{contents}$ is the same as our ArrayList from lab (i.e. the element at index 0 is $\texttt{null}$). 

\bigskip
\noindent
Our goal will be a new constructor for this minHeap class. We will start with a simple helper method, which will make this process simpler. Implement the following $\texttt{height}$ method, which returns the height of the minHeap ($\textit{hint: a minHeap with 7 elements has a height of 2}$).

\bigskip
\begin{lstlisting}
    private int height() {
        _______________;
        _______________;
        while (_________) {
            _______________;
            _______________;
        }
        return _________;
    }
\end{lstlisting}
\newpage
\noindent
Now we are ready to implement our new constructor! Assume your above height method works correctly, and that the Minheaps we are joining are both perfectly balanced and of equal size. In other words, assume the size of both Minheaps is $2^k - 1$, where $k$ is some arbitrary positive integer. 

\bigskip
\noindent
To approach this conceptually, try to draw out two equally-sized, perfectly-balanced minHeaps and think about what changes you could make to retain our Completeness and minHeap properties. 

\bigskip
\noindent
A naive solution would be to iteratively $\texttt{removeMin}$ from each heap and $\texttt{insert}$ into the new heap. This would result in a runtime of $\mathcal{O}(N\log(N))$, where $N$ is the total number of elements in both heaps. We can actually do better! Your solution below should instead take $\Theta(N)$ time.

\bigskip

\ifsolutions{ \color{red}
    \begin{lstlisting}
    public MinHeap(MinHeap<T> h1, MinHeap<T> h2) {
            T[] contents1 = h1.contents;
            T[] contents2 = h2.contents;
            T[] newContents = (T[]) new Comparable[h1.size + h2.size + 2];
            int index = 2;
            for (int layer = 0; layer <= h1.height(); layer++) {
                int numItemsInLayer = (int) Math.pow(2,layer);
                for (int j = 0; j < numItemsInLayer; j++) {
                    newContents[index] = contents1[numItemsInLayer + j];
                    index++;
                }
                for (int j = 0; j < numItemsInLayer; j++) {
                    newContents[index] = contents2[numItemsInLayer + j];
                    index++;
                }
            }
            newContents[1] = newContents[newContents.length - 1];
            newContents[index-1] = null;
            contents = newContents;
            size = h1.size + h2.size;
            bubbleDown(1);
        }
    \end{lstlisting}
}
\else{
    \begin{lstlisting}
    public MinHeap(MinHeap<E> h1, MinHeap<E> h2) {
            E[] contents1 = _______________;
            E[] contents2 = _______________;
            E[] newContents = (E[]) new Comparable[_____________________];
            int index = _______;
            for (_____________________________________________) {
                int numItemsInLayer = (int) Math.pow(_______,_______);
                for (int j = 0; j < ______________; j++) {
                    _____________________________________________________;
                    index++;
                }
                for (int j = 0; j < ______________; j++) {
                    _____________________________________________________;
                    index++;
                }
            }
            newContents[_________] = newContents[_______________________];
            newContents[_________] = null;
            contents = newContents;
            size = ____________________;
            bubbleDown(_____);
        }
    \end{lstlisting}
}
\fi
\end{document}
