# 61B(L) Notes
### Author: Joe Deatrick

LaTeX handout notes and review exercises for Berkeley's Introductory Data Structures and Algorithms Course.

Developed through 4 semesters of experience on course staff.

## Contents:

  ### Handouts:

  - [Why A* Graph Search Requires a consistent heuristic function](https://github.com/Jldeatrick/61B-notes/blob/master/Handouts/A-star/A-star-consistency.pdf "A* Consistency")
  - [An Introduction to P, NP, and NP-Completeness](https://github.com/Jldeatrick/61B-notes/blob/master/Handouts/Complexity/PNP.pdf "P and NP")
  - [Enigma mechanical overview](https://github.com/Jldeatrick/61B-notes/blob/master/Handouts/Enigma/Enigma-mech.pdf "Enigma Slides I")
  - [Enigma skeleton overview](https://github.com/Jldeatrick/61B-notes/blob/master/Handouts/Enigma/Enigma-skeleton.pdf "Enigma Slides II")

  ### Exercises:

  - [CS 61BL Su18 Midterm 3 Review](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/Review/su18/Midterm3_Review.pdf "Midterm 3 Review")
    [[Solutions](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/Review/su18/Midterm3_Review_Sol.pdf)]
  - [CS 61BL Su18 Final Review](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/Review/su18/Final_Review.pdf "Final Review")[[Solutions](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/Review/su18/Final_Review_Sol.pdf)]
  - [CS 61BL Su19 Midterm 2 Review](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/Review/su19/Midterm2_review.pdf "Midterm 2 Review")
  - [LinkedList Exercise - Machete](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/LinkedList/Machete.pdf "Machete")
    [[Solutions](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/LinkedList/Machete_Sol.pdf)]
  - [IntList Exercise - TailSum](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/IntList/TailSum.pdf "TailSum")
    [[Solutions](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/IntList/TailSum_Sol.pdf)]
  - [Java Exercise - Prog](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/Java/Prog.pdf "Prog")
    [[Solutions](https://github.com/Jldeatrick/61B-notes/blob/master/Exercises/Java/Prog_Sol.pdf)]
